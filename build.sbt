name := "ScalaSample"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies += "io.spray" %% "spray-can" % "1.3.3"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.0-RC3"

libraryDependencies += "io.spray" %% "spray-routing" % "1.3.3"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.3.0.RC6"

libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.3.0.RC6"

libraryDependencies += "com.typesafe.slick" %% "slick" % "3.0.3"
