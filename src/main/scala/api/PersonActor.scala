package api

import akka.actor.Actor
import spray.routing.RequestContext

/**
 * Represents a person entry
 */
case class Person(id: Int, name: String, email: String)

/**
 * Service that processes person requests
 */
class PersonActor(requestContext: RequestContext) extends Actor {
  def receive = {
    case personId =>
      // TODO(darin): query slick database for a person with specific id and return it as json
  }
}
