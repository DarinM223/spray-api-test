package api

import java.util.concurrent.TimeUnit

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import org.json4s.{FieldSerializer, Extraction, DefaultFormats}
import spray.http.HttpResponse
import spray.http.HttpEntity.{Empty, NonEmpty}
import spray.routing.{RequestContext}
import akka.io.IO
import spray.can.Http

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Represents an anime entry
 */
case class Anime(
    id: Int,
    mal_id: Int,
    slug: String,
    title: String,
    alternate_title: String,
    episode_count: Int,
    episode_length: Int,
    synopsis: String,
    show_type: String)

/**
 * Services that processes anime requests
 */
class AnimeActor(requestContext: RequestContext) extends Actor {
  import spray.httpx.RequestBuilding._
  import org.json4s.jackson.JsonMethods._
  import org.json4s.FieldSerializer._

  implicit val system = context.system

  def receive = {
    case animeId =>
      // call hummingbird api and return json

      // Implicit value for Get function
      implicit val timeout = Timeout(FiniteDuration(1, TimeUnit.MINUTES))
      val response: Future[HttpResponse] = (IO(Http) ? Get("http://hummingbird.me/api/v1/anime/" + animeId)).mapTo[HttpResponse]

      response onSuccess { case response =>
        response.entity match {
          case nonEmpty: NonEmpty =>
            // Anime JSON serializer renames "title" to "anime_title" and ignores "mal_id"
            val animeSerializer = FieldSerializer[Anime](renameTo("title", "anime_title") orElse ignore("mal_id"))

            // Implicit value for extract and Extraction.decompose
            implicit val formats = DefaultFormats + animeSerializer

            val anime = parse(nonEmpty.asString).extract[Anime]

            requestContext.complete(compact(render(Extraction.decompose(anime))))
          case Empty => println("No response")
        }
      }

      response onFailure { case e =>
        println("An error has occurred: " + e.getMessage)
      }
  }
}
