package api

import akka.actor.{Props, ActorRefFactory, Actor}
import spray.routing.HttpService

/**
 * Supervisor actor that maintains the routes
 */
class APISupervisorActor extends Actor with APISupervisorService {
  def actorRefFactory = context
  def receive = HttpService.runRoute(apiRoutes)
}

trait APISupervisorService extends HttpService {
  val apiRoutes =
    pathPrefix("api") {
      path("getPerson" / IntNumber) { personId =>
        requestContext =>
          val personActor = actorRefFactory.actorOf(Props(new PersonActor(requestContext)))
          personActor ! personId
      } ~
      path("getAnime" / IntNumber) { animeId =>
        requestContext =>
          val animeActor = actorRefFactory.actorOf(Props(new AnimeActor(requestContext)))
          animeActor ! animeId
      }
    }
}
