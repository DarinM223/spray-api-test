import akka.actor.{Props, ActorSystem}
import akka.io.IO
import api.APISupervisorActor
import spray.can.Http

/**
 * Main application class
 * Creates the supervisor actor and binds it to a port
 */
object Main extends App {
  // `system` has to be implicit because then it will be passed into IO(Http)
  // IO(Http)'s function is really def IO(val Http)(implicit val system) so it needs a system
  // parameter
  implicit val system = ActorSystem("ActorSystem")

  val supervisor = system.actorOf(Props[APISupervisorActor], "APISupervisorActor")
  IO(Http) ! Http.Bind(supervisor, interface = "localhost", port = 3000)
}
